class CreateCourses < ActiveRecord::Migration[5.2]
  def up
    create_table :courses do |t|
      t.string :name, null: false
      t.integer :code, null: false, option: 'PRIMARY KEY'

      t.timestamps
    end
  end
  def down
    execute "DROP TABLE courses"
  end
end
