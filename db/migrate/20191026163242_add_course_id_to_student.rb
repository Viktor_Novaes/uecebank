class AddCourseIdToStudent < ActiveRecord::Migration[5.2]
  def up
    add_reference :students, :course, foreign_key: true
  end
  def down
    remove_reference :students, :course, foreign_key: true
  end
end
