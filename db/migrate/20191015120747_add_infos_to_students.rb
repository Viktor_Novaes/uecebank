class AddInfosToStudents < ActiveRecord::Migration[5.2]
  def up
    # execute ALTER TABLE "students" ADD "number_account" varchar(7) NOT NULL
    add_column :students, :number_account, :string, limit: 7, null: false
    # execute ALTER TABLE "students" ADD "cpf" varchar(11) NOT NULL
    add_column :students, :cpf, :string , limit: 11, null: false
    # execute ALTER TABLE "students" ADD "balance" decimal(,2) NOT NULL
    add_column :students, :balance, :decimal, precision: 10, scale: 2
    # execute ALTER TABLE "students" ADD "name" varchar
    add_column :students, :name, :string, null: false
  end

  def down
    remove_column :students, :number_account, :string, limit: 7, null: false
    remove_column :students, :cpf, :string , limit: 11, null: false
    remove_column :students, :balance, :decimal, scale: 2
    remove_column :students, :name, :string, null: false
  end
end
