class AddSendersRecipientToTransactions < ActiveRecord::Migration[5.2]
  def up
    execute "ALTER TABLE transactions ADD CONSTRAINT fk_sender FOREIGN KEY (sender_id) REFERENCES students(id);"
    execute "ALTER TABLE transactions ADD CONSTRAINT fk_recipient FOREIGN KEY (recipient_id) REFERENCES students(id);"
    add_column :transactions, :number_account_recipient, :string, limit: 7, null: false
    add_column :transactions, :cpf_recipient, :string , limit: 11, null: false
  end
  def down
    remove_reference :transactions, :sender
    remove_reference :transactions, :recipient
    # remove_column :transactions, :cpf_recipient,
    # remove_column :transactions, :number_account_recipient,
  end
end
