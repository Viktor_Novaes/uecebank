class CreateTransactions < ActiveRecord::Migration[5.2]
  def up
    create_table :transactions do |t|
      t.decimal :value, precision: 10, scale: 2, null: false
      t.bigint :sender_id
      t.bigint :recipient_id

      t.timestamps
    end
  end
  def down
    execute "DROP TABLE transactions"
  end
end
