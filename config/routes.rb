Rails.application.routes.draw do
  devise_for :students, controllers: {
    sessions: 'students/sessions',
    passwords: 'students/passwords',
    confirmations: 'students/confirmations',
    unlocks: 'students/unlocks',
    registrations: 'students/registrations'
  }
  
  root to: 'client_backoffice/home#index'

  # Paths Authenticate
  namespace 'client_backoffice', path: 'auth' do
    resources :transactions, only: %i[new create index]
    put 'transactions', to: 'transactions#index'
  end
end
