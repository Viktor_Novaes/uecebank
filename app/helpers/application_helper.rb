module ApplicationHelper
  def number_to_currency_br(number)
    number_to_currency(number, :unit => "R$ ", :separator => ",", :delimiter => ".")
  end

  def date_br(date)
    "#{date.strftime("%d/%m/%Y")} as #{date.hour}h#{date.min}m"
  end
end
