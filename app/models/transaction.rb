class Transaction < ApplicationRecord
  belongs_to :sender, class_name: "Student", foreign_key: "sender_id" 
  belongs_to :recipient, class_name: "Student", foreign_key: "recipient_id" 

  validates :cpf_recipient, :number_account_recipient, :value, presence: true
  validate :validateTransaction
  
  before_commit :setBalances, on: %i[create]
  before_validation :setRecipient, on: %i[create]

  scope :by_student, -> (current_student) { where(sender: current_student).or(where(recipient: current_student)) }

  def setRecipient
    @student_recipient = Student.find_by(number_account: self.number_account_recipient)
    self.recipient_id = @student_recipient.id if @student_recipient
  end

  def validateTransaction
    @sender = Student.find(self.sender_id)
    unless self.value.nil?
      errors.add(:base, 'Você não tem saldo em conta suficiente para essa transação') if @sender.balance < self.value
      errors.add(:base, 'Você não fazer transação com esse valor') if self.value <= 0
    end
    errors.add(:base, 'Estudante não encontrado. Por favor verifique as informações da conta') unless @student_recipient
  end

  def setBalances
    @sender.balance -= self.value
    @student_recipient.balance += self.value
    @sender.save!
    @student_recipient.save!
    puts ">>>>>>>>>>>>>>>>>>>>> SETBALANCES <<<<<<<<<<<<<<<<<<<<<<<<<<<"
    puts "#{@sender.balance}"
    puts "#{@student_recipient.balance}"
  end
end
