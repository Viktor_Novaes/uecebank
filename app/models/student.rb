class Student < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :course
  has_many :transactions
  
  # Validates
  validates :cpf, uniqueness: { uniqueness: true, message: 'CPF já cadastrado' }
  validates :number_account, uniqueness: true , presence: true
  validates :name,
          :cpf,
          :email,
          presence: true

  # Callbacks
  before_validation :set_default_settings, on: :create
  
  def set_default_settings
    number = set_number_account()
    set_number_account if Student.find_by(number_account: number)
    
    self.number_account = number
    self.balance = 0.00
  end

  def set_number_account
    number = rand(1000000..9999999)
  end
end
