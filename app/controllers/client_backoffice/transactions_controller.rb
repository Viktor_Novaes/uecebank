class ClientBackoffice::TransactionsController < ClientBackofficeController
  def new
    @transaction = Transaction.new
  end

  def create
    @transaction = Transaction.new(transaction_params)
    @transaction.sender_id = current_student.id
    @transaction.save!
    redirect_to root_path, notice: 'Transação finalizada com sucesso'
    rescue ActiveRecord::RecordInvalid => message
      render :new, alert: message, status: :unprocessable_entity    
  end

  def index
    puts params[:created_at]
    if params[:date_in].present? and params[:date_out].present?
      @extract = Transaction.by_student(current_student).where(created_at: params[:data_in]..params[:data_out]).order(created_at: :asc)
    else
      @extract = Transaction.by_student(current_student)
    end

  end

  private

  def transaction_params
    params.require(:transaction).permit(:value, :sender_id, :recipient_id, :cpf_recipient, :number_account_recipient)
  end
end
