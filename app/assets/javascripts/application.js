// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require activestorage
//= require jquery.min.js
//= require tether.min.js
//= require bootstrap.min.js
//= require jquery.slimscroll
//= require waves
//= require sidebarmenu
//= require sticky-kit.min.js
//= require jquery.flot.js
//= require jquery.flot.tooltip.min.js
//= require flot-data.js
//= require jQuery.style.switcher.js
//= require jquery-ui.min.js


$(function() {

  $(window).load(function() {
    // Animate loader off screen
    $(".preloader").fadeOut("slow");
  });



  jQuery(document).on('click', '.mega-dropdown', function(e) {
    e.stopPropagation()
  });
  // ==============================================================
  // This is for the top header part and sidebar part
  // ==============================================================
  var set = function() {
    var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
    var topOffset = 70;
    if (width < 500) {
      $("body").addClass("mini-sidebar");
      $('.navbar-brand span').hide();
      $(".scroll-sidebar, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible");
      $(".sidebartoggler i").addClass("ti-menu");
    } else {
      $("body").removeClass("mini-sidebar");
      $('.navbar-brand span').show();
      $(".sidebartoggler i").removeClass("ti-menu");
    }

    var height = ((window.innerHeight > 0) ? window.innerHeight : this.screen.height) - 1;
    height = height - topOffset;
    if (height < 1) height = 1;
    if (height > topOffset) {
      $(".page-wrapper").css("min-height", (height) + "px");
    }

  };
  $(window).ready(set);
  $(window).on("resize", set);

  // topbar stickey on scroll

  $(".fix-header .topbar").stick_in_parent({

  });

  // this is for close icon when navigation open in mobile view
  $(".nav-toggler").click(function() {
    $("body").toggleClass("show-sidebar");
    $(".nav-toggler i").toggleClass("ti-menu");
    $(".nav-toggler i").addClass("ti-close");
  });
  $(".sidebartoggler").on('click', function() {
    $(".sidebartoggler i").toggleClass("ti-menu");
  });

  // ==============================================================
  // Auto select left navbar
  // ==============================================================
  $(function() {
    var url = window.location;
    var element = $('ul#sidebarnav a').filter(function() {
      return this.href == url;
    }).addClass('active').parent().addClass('active');
    while (true) {
      if (element.is('li')) {
        element = element.parent().addClass('in').parent().addClass('active');
      } else {
        break;
      }
    }
  });

  // ==============================================================
  // Sidebarmenu
  // ==============================================================
  $(function() {
    $('#sidebarnav').metisMenu();
  });
  // ==============================================================
  // Slimscrollbars
  // ==============================================================
  $('.scroll-sidebar').slimScroll({
    position: 'left',
    size: "5px",
    height: '100%',
    color: '#dcdcdc'
  });

  // ==============================================================
  // Resize all elements
  // ==============================================================
  $("body").trigger("resize");
});

$("td[data-link]").click(function() {
  window.location = $(this).data("link")
})

$( function() {
    $( "#dialog" ).dialog();
} );
