# frozen_string_literal: true

require 'creek'
require 'down'

namespace :populate_courses do
  desc "Create Courses in Database"
  task v1: :environment do
    # Read file import datas
    xls_file = Down.download(ENV['REMOTE_XLS_URL'])
    workbook = Creek::Book.new(xls_file, check_file_extension: false)
    course_sheet = workbook.sheets.first

    # variables environment 
    system('clear')
    fails = 0
    success = 0
    lines_fails = []
    
    puts "\n\n ----- Creating Courses ----- \n"
    course_sheet.rows.each_with_index do |row, index|
      @index = index
      @row = row # => {"A1"=>"Content 1", "B1"=>nil, "C1"=>nil, "D1"=>"Content 3"}
      next if row.empty? || index.zero?
      
      @course = Course.new(
        code: row_id('A').to_i,
        name: row_id('B'),
      )
      puts "\t |#{@course.code} - #{@course.name}"
      
      unless @course.valid? 
        puts "\n\t|Invalid Course: #{@index + 1} - #{@cli.name}"
        puts "\t|Message: #{@course.errors.full_messages}"
        lines_fails << @index + 1
        fails += 1
        next
      end
      
      @course.save!
      success += 1
    end
    puts <<~"RESUME"
    Resume------------:
    Object save successfull: #{success}
    Object failure: #{fails} -> Lines: #{lines_fails}
    RESUME

  end
  
  # Helpers
  def row_id(letter)
    return if @row.nil?

    @row["#{letter}#{@index + 1}"]
  end
end
